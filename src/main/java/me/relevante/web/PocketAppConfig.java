package me.relevante.web;

import me.relevante.api.OAuthKeyPair;
import me.relevante.network.Pocket;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by daniel-ibanez on 24/07/16.
 */
@Configuration
public class PocketAppConfig {

    @Bean
    public OAuthKeyPair<Pocket> pocketMainOAuthKeyPair(@Value("${pocket.oAuthCredentials.main.apiKey}") String key) {
        return new OAuthKeyPair<>(key, null);
    }

}
