package me.relevante.web.persistence;

import me.relevante.model.PocketFullUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PocketFullUserRepo extends MongoRepository<PocketFullUser, String> {
}