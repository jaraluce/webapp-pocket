package me.relevante.web.service;

import me.relevante.api.NetworkCredentials;
import me.relevante.api.NetworkOAuthCredentials;
import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.model.PocketFullUser;
import me.relevante.model.PocketProfile;
import me.relevante.model.RelevanteAccount;
import me.relevante.network.Pocket;
import me.relevante.web.model.RegisterResult;
import me.relevante.web.model.oauth.PocketOAuthManager;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class PocketAccountService implements NetworkAccountService<Pocket, PocketFullUser> {

    private RelevanteAccountRepo relevanteAccountRepo;
    private RelevanteContextRepo relevanteContextRepo;
    private CrudRepository<PocketFullUser, String> fullUserRepo;
    private OAuthKeyPair<Pocket> mainOAuthKeyPair;

    @Autowired
    public PocketAccountService(RelevanteAccountRepo relevanteAccountRepo,
                                RelevanteContextRepo relevanteContextRepo,
                                CrudRepository<PocketFullUser, String> fullUserRepo,
                                OAuthKeyPair<Pocket> mainOAuthKeyPair) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.fullUserRepo = fullUserRepo;
        this.mainOAuthKeyPair = mainOAuthKeyPair;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public PocketFullUser getNetworkUser(RelevanteAccount relevanteAccount) {
        if (!relevanteAccount.isNetworkConnected(Pocket.getInstance())) {
            return null;
        }
        NetworkCredentials credentials = relevanteAccount.getCredentials(Pocket.getInstance());
        PocketFullUser fullUser = fullUserRepo.findOne(credentials.getUserId());
        return fullUser;
    }

    @Override
    public void unregisterNetworkAccount(String relevanteId) {
        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        relevanteAccount.removeCredentials(Pocket.getInstance());
        relevanteAccountRepo.save(relevanteAccount);
    }

    @Override
    public RegisterResult<PocketFullUser> registerOAuthNetworkAccount(String existingRelevanteId,
                                                                      String oAuthRequestToken,
                                                                      String oAuthRequestSecret,
                                                                      String oAuthRequestOriginalToken,
                                                                      String oAuthRequestVerifier) {
        if (existingRelevanteId == null) {
            throw new IllegalArgumentException();
        }

        if (!oAuthRequestToken.equals(oAuthRequestToken)) {
            throw new IllegalArgumentException("Callback token is wrong");
        }

        PocketOAuthManager oAuthManager = new PocketOAuthManager(mainOAuthKeyPair);
        OAuthTokenPair<Pocket> oAuthTokenPair = oAuthManager.obtainAccessToken(oAuthRequestToken, oAuthRequestSecret, oAuthRequestVerifier);
        if (oAuthTokenPair == null) {
            throw new IllegalArgumentException("Couldn't get " + Pocket.getInstance().getName() + " oauth access token");
        }

        NetworkCredentials<Pocket> networkCredentials = new NetworkOAuthCredentials<>(Pocket.getInstance(), oAuthTokenPair.getSecret(), mainOAuthKeyPair, oAuthTokenPair);
        RelevanteAccount existingAccount = relevanteAccountRepo.findOne(existingRelevanteId);
        existingAccount.putCredentials(networkCredentials);

        PocketFullUser fullUser = new PocketFullUser(new PocketProfile(networkCredentials.getUserId()));
        fullUserRepo.save(fullUser);
        populateRelevanteAccountWithNetworkData(existingAccount, fullUser);
        relevanteAccountRepo.save(existingAccount);

        return new RegisterResult<>(existingAccount, fullUser, false);
    }

    @Override
    public RegisterResult<PocketFullUser> registerBasicAuthNetworkAccount(String existingRelevanteId,
                                                                          String url,
                                                                          String username,
                                                                          String password) {
        throw new UnsupportedOperationException();
    }

    protected void populateRelevanteAccountWithNetworkData(RelevanteAccount relevanteAccount,
                                                           PocketFullUser fullUser)  {

        if (relevanteAccount.getName() == null) {
            relevanteAccount.setName(fullUser.getProfile().getUsername());
        }
    }

}
