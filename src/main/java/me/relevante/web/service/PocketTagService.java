package me.relevante.web.service;

import me.relevante.api.NetworkOAuthCredentials;
import me.relevante.api.PocketApi;
import me.relevante.api.PocketApiImpl;
import me.relevante.model.NetworkEntity;
import me.relevante.model.PocketDetailType;
import me.relevante.model.PocketSearchTerms;
import me.relevante.model.RelevanteAccount;
import me.relevante.network.Network;
import me.relevante.network.Pocket;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PocketTagService implements NetworkEntity {

    private RelevanteAccountRepo relevanteAccountRepo;

    @Autowired
    public PocketTagService(RelevanteAccountRepo relevanteAccountRepo) {
        this.relevanteAccountRepo = relevanteAccountRepo;
    }

    @Override
    public Network getNetwork() {
        return Pocket.getInstance();
    }

    public List<String> getAll(String relevanteId) {
        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkOAuthCredentials<Pocket> pocketCredentials = (NetworkOAuthCredentials) relevanteAccount.getCredentials(Pocket.getInstance());
        PocketApi pocketApi = new PocketApiImpl(pocketCredentials.getOAuthConsumerKeyPair(), pocketCredentials.getOAuthAccessTokenPair());
        PocketSearchTerms searchTerms = new PocketSearchTerms();
        searchTerms.setDetailType(PocketDetailType.COMPLETE);
        List<String> tags = pocketApi.getTags(searchTerms);
        return tags;
    }
}
