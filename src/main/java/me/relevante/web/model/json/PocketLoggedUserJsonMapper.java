package me.relevante.web.model.json;

import me.relevante.model.PocketProfile;
import me.relevante.network.Pocket;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class PocketLoggedUserJsonMapper implements NetworkLoggedUserJsonMapper<Pocket, PocketProfile> {

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public JSONObject mapNetworkProfileToJson(PocketProfile profile) {

        JSONObject jsonProfile = new JSONObject();
        jsonProfile.put("username", profile.getUsername());
        return jsonProfile;
    }
}