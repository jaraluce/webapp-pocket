package me.relevante.web.model.oauth;

import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.network.Pocket;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PocketOAuthManager implements NetworkOAuthManager<Pocket> {

    private static final Logger logger = LoggerFactory.getLogger(PocketOAuthManager.class);
    private static final String REQUEST_URL = "https://getpocket.com/v3/oauth/request";
    private static final String OAUTH_URL = "https://getpocket.com/auth/authorize?request_token=%s&redirect_uri=%s";
    private static final String AUTHORIZE_URL = "https://getpocket.com/v3/oauth/authorize";

    private OAuthKeyPair<Pocket> oAuthConsumerKeyPair;

    @Autowired
    public PocketOAuthManager(OAuthKeyPair<Pocket> oAuthConsumerKeyPair) {
        this.oAuthConsumerKeyPair = oAuthConsumerKeyPair;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public OAuthKeyPair<Pocket> getOAuthConsumerKeyPair() {
        return oAuthConsumerKeyPair;
    }

    @Override
    public OAuthAccessRequest<Pocket> createOAuthRequest(String callbackUrl) {

        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(REQUEST_URL);

        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        httpPost.addHeader("X-Accept", "application/x-www-form-urlencoded");

        List<NameValuePair> params = new ArrayList<>(2);
        params.add(new BasicNameValuePair("consumer_key", oAuthConsumerKeyPair.getKey()));
        params.add(new BasicNameValuePair("redirect_uri", callbackUrl));

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);
            OAuthAccessRequest<Pocket> oAuthAccessRequest = createOAuthRequestFromResponse(response, callbackUrl);
            return oAuthAccessRequest;
        } catch (Exception e) {
            logger.error("Error", e);
        }
        return null;
    }

    @Override
    public OAuthTokenPair<Pocket> obtainAccessToken(String requestTokenString,
                                                    String requestSecret,
                                                    String requestVerifierString) {
        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(AUTHORIZE_URL);

        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        httpPost.addHeader("X-Accept", "application/x-www-form-urlencoded");

        List<NameValuePair> params = new ArrayList<>(2);
        params.add(new BasicNameValuePair("consumer_key", oAuthConsumerKeyPair.getKey()));
        params.add(new BasicNameValuePair("code", requestTokenString));

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);
            OAuthTokenPair<Pocket> oAuthAccessToken = createOAuthTokenFromResponse(response);
            return oAuthAccessToken;
        } catch (Exception e) {
            logger.error("Error", e);
        }
        return null;
    }

    private OAuthAccessRequest<Pocket> createOAuthRequestFromResponse(HttpResponse response,
                                                                      String redirectUri) throws IOException {
        HttpEntity entity = response.getEntity();
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        InputStream inputStream = entity.getContent();
        OAuthAccessRequest<Pocket> oAuthAccessRequest = null;
        try {
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
            String theString = URLDecoder.decode(writer.toString(), "UTF-8");
            String[] items = theString.split("=");
            String oAuthUrl = String.format(OAUTH_URL, items[1], redirectUri);
            oAuthAccessRequest = new OAuthAccessRequest<>(items[1], null, oAuthUrl);
        }
        finally {
            inputStream.close();
        }
        return oAuthAccessRequest;
    }

    private OAuthTokenPair<Pocket> createOAuthTokenFromResponse(HttpResponse response) throws IOException {
        HttpEntity entity = response.getEntity();
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        InputStream inputStream = entity.getContent();
        OAuthTokenPair<Pocket> oAuthTokenPair = null;
        try {
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
            String theString = URLDecoder.decode(writer.toString(), "UTF-8");
            String[] params = theString.split("&");
            Map<String, String> paramsMap = new HashMap<>();
            for (String param : params) {
                String[] items = param.split("=");
                paramsMap.put(items[0].toLowerCase(), items[1]);
            }
            oAuthTokenPair = new OAuthTokenPair<>(paramsMap.get("access_token"), paramsMap.get("username"));
        }
        finally {
            inputStream.close();
        }
        return oAuthTokenPair;
    }
}
