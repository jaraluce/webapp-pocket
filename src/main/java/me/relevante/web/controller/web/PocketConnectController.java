package me.relevante.web.controller.web;

import me.relevante.network.Pocket;
import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.PocketRoute;
import me.relevante.web.model.oauth.PocketOAuthManager;
import me.relevante.web.model.oauth.OAuthAccessRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class PocketConnectController {

    @Autowired private PocketOAuthManager pocketOAuthManager;

    @RequestMapping(value = PocketRoute.CONNECT, method = RequestMethod.GET)
    public ModelAndView get(@RequestParam(name = "redirectUri") String redirectUri,
                            HttpServletRequest request) {

        String baseUrl = request.getRequestURL().substring(0, request.getRequestURL().indexOf(PocketRoute.CONNECT));
        String callbackUrl = baseUrl + PocketRoute.CONNECT_CALLBACK;
        OAuthAccessRequest<Pocket> oAuthAccessRequest = pocketOAuthManager.createOAuthRequest(callbackUrl);
        HttpSession session = request.getSession();
        session.setAttribute(CoreSessionAttribute.OAUTH_REQUEST_TOKEN, oAuthAccessRequest.getRequestToken());
        session.setAttribute(CoreSessionAttribute.REDIRECT_URI, redirectUri);

        return new ModelAndView("redirect:" + oAuthAccessRequest.getOAuthUrl());
    }

}
