package me.relevante.web.controller.web;

import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.PocketRoute;
import me.relevante.web.service.PocketAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RestController
public class PocketConnectCallbackController {

    @Autowired private PocketAccountService accountService;

    @RequestMapping(value = PocketRoute.CONNECT_CALLBACK, method = RequestMethod.GET)
    public ModelAndView get(HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        String oAuthRequestToken = (String) session.getAttribute(CoreSessionAttribute.OAUTH_REQUEST_TOKEN);
        String redirectUri = (String) session.getAttribute(CoreSessionAttribute.REDIRECT_URI);

        accountService.registerOAuthNetworkAccount(relevanteId, oAuthRequestToken, null, null, null);

        return new ModelAndView("redirect:" + redirectUri);
    }

}
