package me.relevante.web.controller.api;

import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.PocketRoute;
import me.relevante.web.service.PocketTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = PocketRoute.API_TAGS)
public class PocketTagController {

    @Autowired private PocketTagService tagService;

    @RequestMapping(method = RequestMethod.GET)
    public List<String> getAll(HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<String> tags = tagService.getAll(relevanteId);
        return tags;
    }

}
