package me.relevante.web.controller;

public class PocketRoute {

    public static final String NETWORK = "/pocket";
    public static final String CONNECT = NETWORK + "/connect";
    public static final String CONNECT_CALLBACK = CONNECT + "/callback";
    public static final String DISCONNECT = NETWORK + "/disconnect";
    public static final String API_TAGS = CoreRoute.API_PREFIX + NETWORK + "/tags";
}

